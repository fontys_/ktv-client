package nl.fontys.moc.rest;

/**
 *
 * @author Gerrit Drost <mail@gerritdrost.com>
 */
public class AuthenticationFailedException extends RuntimeException {
    private String username;
    private String password;

    public AuthenticationFailedException(String username, String password) {
        super("Authentication using username '" + username + "' and password '" + password + "' failed");
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
