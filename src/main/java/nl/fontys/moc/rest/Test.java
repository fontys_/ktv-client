package nl.fontys.moc.rest;

import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import com.owlike.genson.Genson;
import nl.fontys.moc.rest.models.*;

/**
 *
 * @author Gerrit Drost <mail@gerritdrost.com>
 */
public class Test {
public static void main(String[] args) {    
        Client client = new Client("http://172.16.197.128:8083/api", "test", "test");
        List<Assignment> assignments = client.getAssignmentService().getAssignments();
        User currentUser = client.getUserService().getCurrentUser();

        long timestamp = Instant.now().getEpochSecond();
        String timestampString = Long.toString(timestamp);

        HashSet<Member> members = new HashSet<>();

        String teamName = "team" + timestampString;
        String userName = "user" + timestampString;
        String password = "pass" + timestampString;
        String fullName = "Full Name" + timestampString;
        String emailAddress = userName + "@" + teamName + ".com";

        Random random = new Random();
        int membersCount = random.nextInt(5);

        for (int i = 0; i < membersCount; i++) {
                String memberName = "member" + Long.toString(timestamp + i + 1);
                members.add(new Member(teamName, memberName, memberName + "@" + teamName + ".com"));
        }

        User newTeam = new User(
                userName,
                password,
                fullName,
                teamName,
                emailAddress,
                "team", // can also be "admin" or "guest"
                members,
                null,
                random.nextInt(1200)
        );
        Client.UserService userService = client.getUserService();

        User createdTeam = userService.createUser(newTeam);
        List<User> users = userService.getUsers();

        System.out.println("done");

//        List<Competition> competitions = client.getCompetitionService().getCompetitions();
//        List<Node> nodes = client.getNodeService().getNodes();
//        List<Round> rounds = client.getRoundService().getRounds();
//        List<Score> scores = client.getScoreService().getScores();
//        List<User> users = client.getUserService().getUsers();
//        
//        User currentUser = client.getUserService().getCurrentUser();
//        
//        System.out.println(currentUser.getFullname());
//        
//        for (Assignment assignment : assignments) {
//            System.out.println(assignment.getName());
//        }
//        
//        for (Competition competition : competitions) {
//            System.out.println(competition.getTitle());
//        }
//        
//        for (Node node : nodes) {
//            System.out.println(node.getJndi());
//        }
//        
//        for (Round round : rounds) {
//            System.out.println(round.getId());
//        }
//        
//        for (Score score : scores) {
//            System.out.println(score.getScore());
//        }
//        
//        for (User user : users) {
//            System.out.println(user.getFullname());
//        }
}
}
