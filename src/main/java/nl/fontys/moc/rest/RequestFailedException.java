package nl.fontys.moc.rest;

/**
 * @author Gerrit Drost <mail@gerritdrost.com>
 */
public class RequestFailedException extends RuntimeException {
    protected int status;
    protected String output;

    public RequestFailedException(int status, String output) {
        super("Request failed with status code " + Integer.toString(status) + ", output from server: " + output);

        this.status = status;
        this.output = output;
    }
}
