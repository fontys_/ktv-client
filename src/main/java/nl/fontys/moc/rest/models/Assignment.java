package nl.fontys.moc.rest.models;

import com.owlike.genson.annotation.JsonProperty;
import java.util.Set;

/**
 * @author Gerrit Drost <mail@gerritdrost.com>
 */
public class Assignment {
    private String id;
    private String artifact;
    private String name;
    private String participantDescription;
    private String spectatorDescription;
    private String creatorName;
    private String creatorOrganisation;
    private String creatorLink;
    private Set<Hint> hints;


    public Assignment( 
            @JsonProperty("id") String id,
            @JsonProperty("artifact") String artifact,
            @JsonProperty("name") String name, 
            @JsonProperty("participantDescription") String participantDescription, 
            @JsonProperty("spectatorDescription") String spectatorDescription, 
            @JsonProperty("creatorName") String creatorName, 
            @JsonProperty("creatorOrganisation") String creatorOrganisation, 
            @JsonProperty("creatorLink") String creatorLink, 
            @JsonProperty("hints") Set<Hint> hints
    ) {
        this.id = id;
        this.artifact = artifact;
        this.name = name;
        this.participantDescription = participantDescription;
        this.spectatorDescription = spectatorDescription;
        this.creatorName = creatorName;
        this.creatorOrganisation = creatorOrganisation;
        this.creatorLink = creatorLink;
        this.hints = hints;
    }
    
    public String getId() {
            return id;
    }

    public void setId(String id) {
            this.id = id;
    }
    
    public String getArtifact() {
            return artifact;
    }

    public void setArtifact(String artifact) {
            this.artifact = artifact;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParticipantDescription() {
        return participantDescription;
    }

    public void setParticipantDescription(String participantDescription) {
        this.participantDescription = participantDescription;
    }

    public String getSpectatorDescription() {
        return spectatorDescription;
    }

    public void setSpectatorDescription(String spectatorDescription) {
        this.spectatorDescription = spectatorDescription;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getCreatorOrganisation() {
        return creatorOrganisation;
    }

    public void setCreatorOrganisation(String creatorOrganisation) {
        this.creatorOrganisation = creatorOrganisation;
    }

    public String getCreatorLink() {
        return creatorLink;
    }

    public void setCreatorLink(String creatorLink) {
        this.creatorLink = creatorLink;
    }

    public Set<Hint> getHints() {
        return hints;
    }

    public void setHints(Set<Hint> hints) {
        this.hints = hints;
    }
}
