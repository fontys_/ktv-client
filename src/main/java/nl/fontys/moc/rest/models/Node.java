package nl.fontys.moc.rest.models;

import com.owlike.genson.annotation.JsonProperty;

/**
 *
 * @author Gerrit Drost <mail@gerritdrost.com>
 */
public class Node {
    private int id;
    private String jndi;

    public Node(
        @JsonProperty("id") int id, 
        @JsonProperty("jndi") String jndi
    ) {
        this.id = id;
        this.jndi = jndi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJndi() {
        return jndi;
    }

    public void setJndi(String jndi) {
        this.jndi = jndi;
    }
}
