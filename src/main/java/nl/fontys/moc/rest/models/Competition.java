package nl.fontys.moc.rest.models;

import com.owlike.genson.annotation.JsonProperty;
import java.util.Set;

/**
 *
 * @author Gerrit Drost <mail@gerritdrost.com>
 */
public class Competition {
    private int id;
    private String title;
    private String description;
    private Set<User> teams;
    private Set<Round> rounds;

    public Competition(
            @JsonProperty("id") int id,
            @JsonProperty("title") String title,
            @JsonProperty("description") String description,
            @JsonProperty("teams") Set<User> teams,
            @JsonProperty("rounds") Set<Round> rounds
    ) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.teams = teams;
        this.rounds = rounds;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<User> getTeams() {
        return teams;
    }

    public void setTeams(Set<User> teams) {
        this.teams = teams;
    }

    public Set<Round> getRounds() {
        return rounds;
    }

    public void setRounds(Set<Round> rounds) {
        this.rounds = rounds;
    }
}
