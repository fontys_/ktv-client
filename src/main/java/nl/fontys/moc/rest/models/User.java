package nl.fontys.moc.rest.models;

import com.owlike.genson.annotation.JsonCreator;
import com.owlike.genson.annotation.JsonProperty;
import java.util.Set;

/**
 *
 * @author Gerrit Drost <mail@gerritdrost.com>
 */
public class User {
    private String username;
    private String password;
    private String fullname;
    private String teamname;
    private String email;
    private String role;
    private Set<Member> members;
    private Node node;
    private int totalscore;
    private String id;

//    @JsonCreator
//    public User(
//            @JsonProperty("username") String username,
//            @JsonProperty("password") String password,
//            @JsonProperty("fullname") String fullname,
//            @JsonProperty("teamname") String teamname,
//            @JsonProperty("email") String email,
//            @JsonProperty("role") String role,
//            @JsonProperty("members") Set<Member> members,
//            @JsonProperty("node") Node node,
//            @JsonProperty("totalscore") int totalscore,
//            @JsonProperty("id") String id
//    ) {
//        this.username = username;
//        this.password = password;
//        this.fullname = fullname;
//        this.teamname = teamname;
//        this.email = email;
//        this.role = role;
//        this.members = members;
//        this.node = node;
//        this.totalscore = totalscore;
//        this.id = id;
//    }

    public User() {

    }

    public User(
            String username,
            String password,
            String fullname,
            String teamname,
            String email,
            String role,
            Set<Member> members,
            Node node,
            int totalscore
    ) {
        this.username = username;
        this.password = password;
        this.fullname = fullname;
        this.teamname = teamname;
        this.email = email;
        this.role = role;
        this.members = members;
        this.node = node;
        this.totalscore = totalscore;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getTeamname() {
        return teamname;
    }

    public void setTeamname(String teamname) {
        this.teamname = teamname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Set<Member> getMembers() {
        return members;
    }

    public void setMembers(Set<Member> members) {
        this.members = members;
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public int getTotalscore() {
        return totalscore;
    }

    public void setTotalscore(int totalscore) {
        this.totalscore = totalscore;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
