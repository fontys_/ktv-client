package nl.fontys.moc.rest.models;

import com.owlike.genson.annotation.JsonProperty;

/**
 *
 * @author Gerrit Drost <mail@gerritdrost.com>
 */
public class Round {
    private int id;
    private int competitionId;
    private int duration;
    private int multiplier;
    private Assignment assignment;

    public Round(
        @JsonProperty("id") int id, 
        @JsonProperty("competition") int competitionId, 
        @JsonProperty("duration") int duration, 
        @JsonProperty("multiplier") int multiplier, 
        @JsonProperty("assignment") Assignment assignment
    ) {
        this.id = id;
        this.competitionId = competitionId;
        this.duration = duration;
        this.multiplier = multiplier;
        this.assignment = assignment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCompetitionId() {
        return competitionId;
    }

    public void setCompetitionId(int competitionId) {
        this.competitionId = competitionId;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(int multiplier) {
        this.multiplier = multiplier;
    }

    public Assignment getAssignment() {
        return assignment;
    }

    public void setAssignment(Assignment assignment) {
        this.assignment = assignment;
    }
}
