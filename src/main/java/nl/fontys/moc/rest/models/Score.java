package nl.fontys.moc.rest.models;

import com.owlike.genson.annotation.JsonProperty;

/**
 *
 * @author Gerrit Drost <mail@gerritdrost.com>
 */
public class Score {
    private int id;
    private User user;
    private Round round;
    private int score;

    public Score(
        @JsonProperty("id") int id,
        @JsonProperty("user") User user,
        @JsonProperty("round") Round round,
        @JsonProperty("score") int score
    ) {
        this.id = id;
        this.user = user;
        this.round = round;
        this.score = score;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Round getRound() {
        return round;
    }

    public void setRound(Round round) {
        this.round = round;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
    
    
}
