package nl.fontys.moc.rest.models;

import com.owlike.genson.annotation.JsonProperty;

/**
 *
 * @author Gerrit Drost <mail@gerritdrost.com>
 */
public class Hint {

    private int id;
    private String assignmentId;
    private String text;
    private int time;

    public Hint(
            @JsonProperty("id") int id,
            @JsonProperty("assignment") String assignmentId,
            @JsonProperty("text") String text,
            @JsonProperty("time") int time
    ) {
        this.id = id;
        this.assignmentId = assignmentId;
        this.text = text;
        this.time = time;
    }

    public Hint(String assignmentId, String text, int time) {
        this.assignmentId = assignmentId;
        this.text = text;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(String assignmentId) {
        this.assignmentId = assignmentId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}
