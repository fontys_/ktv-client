package nl.fontys.moc.rest;

import java.net.URI;
import java.util.List;
import java.util.Map;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import com.owlike.genson.GenericType;
import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;
import com.owlike.genson.reflect.VisibilityFilter;
import nl.fontys.moc.rest.models.Assignment;
import nl.fontys.moc.rest.models.Competition;
import nl.fontys.moc.rest.models.Hint;
import nl.fontys.moc.rest.models.Node;
import nl.fontys.moc.rest.models.Round;
import nl.fontys.moc.rest.models.Score;
import nl.fontys.moc.rest.models.User;
import nl.fontys.moc.rest.models.Member;

import static javax.ws.rs.client.Entity.entity;

/**
 *
 * @author Gerrit Drost <mail@gerritdrost.com>
 */
public class Client {

    protected String username;
    protected String password;

    protected URI mocUri;
    protected WebTarget apiWebTarget;
    
    protected AssignmentService assignmentService = new AssignmentService();
    protected CompetitionService competitionService = new CompetitionService();
    protected NodeService nodeService = new NodeService();
    protected RoundService roundService = new RoundService();
    protected ScoreService scoreService = new ScoreService();
    protected UserService userService = new UserService();

    protected Genson genson = new Genson();

    /**
     * Constructor that will perform requests to mocUrl with the guest user
     *
     * @param mocUrl the url of the masters of code api. For example: "http://localhost:8083/api"
     */
    public Client(String mocUrl) {
        this(mocUrl, "guest", "guest");
    }

    /**
     * Constructor that will perform requests to mocUrl with the provided username and password
     *
     * @param mocUrl the url of the masters of code api. For example: "http://localhost:8083/api"
     * @param username the username to login with
     * @param password the password to login with
     */
    public Client(String mocUrl, String username, String password) {
        setAuthenticationCredentials(username, password);
        setMocUrl(mocUrl);
    }

    /**
     * Sets the mocUrl. After calling this method, all api-calls performed will be on the provided url.
     *
     * @param mocUrl the url the api resides at, for example http://localhost:8083/api
     */
    public void setMocUrl(String mocUrl) {
        this.mocUri = UriBuilder.fromUri(mocUrl).build();
        this.apiWebTarget =  ClientBuilder.newClient().target(mocUri);
    }

    /**
     * Sets the username and password of the client. After calling this method, all api-calls performed will be done using
     * the provided username and password.
     *
     * @param username
     * @param password
     */
    public void setAuthenticationCredentials(String username, String password) {
        this.username = username;
        this.password = password;
    }

    /**
     * Sets the username and password to be used from the next request on
     *
     * @param username the username to use from the next request on
     * @param password the password to use from the next request on
     */
    public void changeCredentials(String username, String password) {
        this.username = username;
        this.password = password;
    }

    /**
     * Sets the guest user to be used from the next request on
     */
    public void changeCredentialsToGuest() {
        this.username = "guest";
        this.password = "guest";
    }

    /**
     * Creates a object of type Invocation.Builder in which the authentication has already been set and the right path
     * has already been selected.
     *
     * @param paths a varargs containing the api path segments. Example:
     *              - "/competitions"  -> getInvocationBuilder("assignments")
     *              - "/users/current" -> getInvocationBuilder("users", "current")
     *
     * @return
     */
    protected Invocation.Builder getInvocationBuilder(String... paths) {
        WebTarget target = apiWebTarget;

        for (String path : paths) {
            target = target.path(path);
        }

        return target
                .request()
                .cookie(requestAuthCookie());
    }

    /**
     *
     * @param genericType a GenericType<\T> required because of genericType erasure
     * @param entity the entity to post with the request
     * @param paths  the path to the right api call translated to an array, e.g. /users/current -> [ "users", "current" ]
     * @param <T> the expected output type
     * @param <U> the type of the entity posted with the request
     * @return
     */
    protected <T,U> T post(GenericType<T> genericType, Entity<U> entity, String... paths) {
        return handleResponse(
                getInvocationBuilder(paths)
                    .accept(MediaType.APPLICATION_JSON)
                    .post(entity, Response.class),
                genericType
        );
    }

    /**
     * Performs a get request
     *
     * @param genericType a GenericType<\T> required because of genericType erasure
     * @param paths the path to the right api call translated to an array, e.g. /users/current -> [ "users", "current" ]
     * @param <T> the expected output type
     * @return
     */
    protected <T> T get(GenericType<T> genericType, String... paths) {
        return handleResponse(
                getInvocationBuilder(paths)
                        .accept(MediaType.APPLICATION_JSON)
                        .get(Response.class),
                genericType
        );
    }

    /**
     * Checks in the response if a request was successful. If not, throws an exception, otherwise returns the desired
     * object.
     *
     * @param response the Response object
     * @param genericType a GenericType<\T> required because of genericType erasure
     * @param <T> the expected output type
     *
     * @return
     */
    protected <T> T handleResponse(Response response, GenericType<T> genericType) {
        String output = response.readEntity(String.class);

        if (response.getStatus() < 200 || response.getStatus() >= 300) {
            throw new RequestFailedException(response.getStatus(), output);
        }

        return genson.deserialize(output, genericType);
    }

    /**
     * Performs a call to security to obtain a session cookie.
     *
     * @return returns a cookie containing the session id
     */
    protected Cookie requestAuthCookie() {
        Response response = apiWebTarget.path("security")
                .request()
                .accept(MediaType.TEXT_PLAIN)
                .post(entity(username + ";" + password, MediaType.TEXT_PLAIN), Response.class);

        Map<String, NewCookie> cookies = response.getCookies();

        if (cookies.containsKey("JSESSIONID")) {
            return cookies.get("JSESSIONID");
        } else {
            throw new AuthenticationFailedException(username, password);
        }
    }
    
    public AssignmentService getAssignmentService() {
        return assignmentService;
    }
    
    public CompetitionService getCompetitionService() {
        return competitionService;
    }
    
    public NodeService getNodeService() {
        return nodeService;
    }
    
    public RoundService getRoundService() {
        return roundService;
    }
    
    public ScoreService getScoreService() {
        return scoreService;
    }
    
    public UserService getUserService() { return userService; }
    
    public class AssignmentService {
        private GenericType<Assignment> genericAssignment = new GenericType<Assignment>() {};
        private GenericType<List<Assignment>> genericAssignmentList = new GenericType<List<Assignment>>() {};

        private AssignmentService() {}
        
        public List<Assignment> getAssignments() {
            return get(genericAssignmentList, "assignments");
        }

        public Assignment getAssignment(String assignmentId) {
            return get(genericAssignment, "assignments", assignmentId);
        }

        public Assignment createAssignment(Assignment assignment) { throw new NotImplementedException(); }
        public Assignment updateAssignment(Assignment assignment) { throw new NotImplementedException(); }
        
        public void deleteAssignment(String assignmentId) { 
            getInvocationBuilder("assignments", assignmentId)
                    .delete(); 
        }
    }
    
    public class CompetitionService {
        private GenericType<Competition> genericCompetition = new GenericType<Competition>() {};
        private GenericType<List<Competition>> genericCompetitionList = new GenericType<List<Competition>>() {};

        private CompetitionService() {}
        
        public List<Competition> getCompetitions() {
            return get(genericCompetitionList, "competitions");
        }

        public Competition getCompetition(Integer competitionId) {
            return get(genericCompetition, "competitions", competitionId.toString());
        }

        public Competition getCurrentCompetition() { 
            return getInvocationBuilder("competitions", "current")
                    .accept(MediaType.APPLICATION_JSON)
                    .get(Competition.class);
        }
        
        public Competition createCompetition(Competition competition) { throw new NotImplementedException(); }
        public Competition updateCompetition(Competition competition) { throw new NotImplementedException(); }
        
        public void deleteCompetition(Integer competitionId) {
            getInvocationBuilder("competitions", competitionId.toString())
                    .delete(Competition.class);
	}
    }
    
    public class NodeService {
        private GenericType<Node> genericNode = new GenericType<Node>() {};
        private GenericType<List<Node>> genericNodeList = new GenericType<List<Node>>() {};

        private NodeService() {}
        
        public List<Node> getNodes() {
            return get(genericNodeList, "nodes");
        }

        public Node getNode(Integer nodeId) {
            return get(genericNode, "nodes", nodeId.toString());
        }

        public Node createNode(Node node) { throw new NotImplementedException(); }
        public Node updateNode(Node node) { throw new NotImplementedException(); }
        
        public void deleteNode(Integer nodeId) {
            getInvocationBuilder("nodes", nodeId.toString())
                    .delete(Competition.class);
	}
    }
    
    public class RoundService {
        private GenericType<Round> genericRound = new GenericType<Round>() {};
        private GenericType<List<Round>> genericRoundList = new GenericType<List<Round>>() {};

        private RoundService() {}
        
        public List<Round> getRounds() {
            return get(genericRoundList, "rounds");
        }

        public Round getRound(Integer roundId) {
            return get(genericRound, "rounds", roundId.toString());
        }

        public Round createRound(Round round) { throw new NotImplementedException(); }
        public Round updateRound(Round round) { throw new NotImplementedException(); }
        
        public void deleteRound(Integer roundId) {
            getInvocationBuilder("rounds", roundId.toString())
                    .delete(Competition.class);
	}
    }
    
    public class ScoreService {
        private ScoreService() {}
        
        public List<Score> getScores() {
            return get(new GenericType<List<Score>>() { }, "scores");
        }
    }

    public class UserService {
        private GenericType<User> genericUser = new GenericType<User>() {};
        private GenericType<List<User>> genericUserList = new GenericType<List<User>>() {};

        private UserService() {}
        
        public List<User> getUsers() {
            return get(genericUserList, "users");
        }

        public User getUser(String username) {
            return get(genericUser, "users", username);
        }

        public User getCurrentUser() {
            return get(genericUser, "users", "current");
        }

        public User createUser(User user) {
            Genson customGenson = (new GensonBuilder())
                    .exclude("id", User.class)
                    .exclude("id", Member.class)
                    .create();

            Entity requestBody = entity(customGenson.serialize(user), MediaType.APPLICATION_JSON);

            Response response = getInvocationBuilder("users")
                    .accept(MediaType.APPLICATION_JSON)
                    .post(requestBody, Response.class);

            String output = response.readEntity(String.class);

            if (response.getStatus() < 200 || response.getStatus() >= 300) {
                throw new RequestFailedException(response.getStatus(), output);
            }

            return post(genericUser, requestBody, "users");
        }

        public User updateUser(User user) {
            return get(genericUser, "users");
        }

        public void deleteUser(Integer userId) {
            get(genericUser, "users", userId.toString());
        }
    }

    public class HintService {
        private GenericType<Hint> genericHint = new GenericType<Hint>() {};
        private GenericType<List<Hint>> genericHintList = new GenericType<List<Hint>>() {};

        private HintService() {}
        
        public List<Hint> getHints() {
            return get(genericHintList, "hints");
        }

        public Hint getHint(Integer hintId) {
            return get(genericHint, "hints", hintId.toString());
        }

        public User createHint(Hint hint) { throw new NotImplementedException(); }
        public User updateHint(Hint hint) { throw new NotImplementedException(); }
        
        public void deleteHint(Integer hintId) {
            getInvocationBuilder("hints", hintId.toString())
                    .delete(Competition.class);
	}
    }
}
